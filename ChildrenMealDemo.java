public class ChildrenMealDemo {

    public static void main(String[] args){

        System.out.println("***Children meal builder demo***");
        Direct employee = new Direct();

        ImealBuilder hamburgerBuilder = new hamburger();

        ImealBuilder cheeseburgerBuilder = new cheeseburger();

        ImealBuilder chickenBuilder = new chicken();

        // making hamburger
        System.out.println("***Making Hamburger***");
        employee.Construct(hamburgerBuilder);
        childrenMeal m1 = hamburgerBuilder.getMeal();
        m1.Show();

        //making cheese burger
        System.out.println("***Making Cheese burger***");
        employee.Construct(cheeseburgerBuilder);
        childrenMeal m2 = cheeseburgerBuilder.getMeal();
        m2.Show();

        //making chicken
        System.out.println("***Making Chicken***");
        employee.Construct(chickenBuilder);
        childrenMeal m3 = chickenBuilder.getMeal();
        m3.Show();

    }

}
